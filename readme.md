# [Dallas Semiconductor DS18B20](http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf) Digital Temperature Sensor Library #
## For Arduino Uno ##

* * *

### INTERFACING ###

The DS18B20 digital temperature sensor has an accuracy of +/- 0.5°C, and precision of up to 0.0625°C, with built-in error detection.  This library implements a CRC check to ensure the integrity of the "1-Wire" serial transmission.

Initializing the sensor requires 1 or 2 arguments: the data pin and the temperature resolution.  Resolution comes in 4 options:

* Low - 0.5°C (0.9°F) precision

* Standard - 0.25°C (0.45°F) precision

* High - 0.125°C (0.225°F) precision

* Ultra - 0.0625°C (0.1125°F) precision (default)

### INITIALIZATION ###

``` {.cpp}
int8_t dataPin = 2;
DallasTemp TempSensor(dataPin); // Default resolution of Ultra
```

Or alternatively
``` {.cpp}
DallasTemp TempSensor(dataPin, DallasTemp::Resolution::Low); // for faster, low resolution readings.
```

* * *

### CHANGING RESOLUTION ###

The sensor's resolution can be changed after initialization by using the following method:
``` {.cpp}
TempSensor.setResolution(DallasTemp::Resolution::Standard);
```

* * *

### GETTING ADDRESS(ES) ###

You can have many sensors on a single IO pin due to the implementation of an address system in the sensor's ROM.  Each address is 64-bits long.  In this example we have only a single sensor on the bus.

Getting address for applications of one sensor on the bus:
``` {.cpp}
// Get address as a string (in hex values)
string addressStr = TempSensor.getAddress(); // example: {0x28, 0xFF, 0x82, 0x50, 0xA3, 0x15, 0x04, 0xB8}
```
``` {.cpp}
// Get address as 8-byte array
uint8_t addressArr[8]; // 64-bit address container
TempSensor.getAddress(addressArr); // Records address to array argument
```

Getting addresses for applications of 2  (or more) sensors on the bus:
``` {.cpp}
// Get addresses as string
string addressesStr = TempSensor.getAddresses();
```
``` {.cpp}
// Get addresses as multiple 8-byte arrays.
// Example assumes there are only 2 sensors on the bus
uint8_t indoorAddr[8];
uint8_t outdoorAddr[8];
bool keepSearching; // Indicates if there is another sensor.
keepSearching = TempSensor.getAddress(indoorAddr); // Should return true indicating another sensor detected.
keepSearching = TempSensor.getAddress(outdoorAddr); // Should return false if all addresses have been found.
```

* * *

### GETTING TEMPERATURE ###

Now that you have your address(es), requesting temperature in Celsius or Fahrenheit is straightforward.

``` {.cpp}
double indoorTemp = TempSensor.getTemperature(indoorAddr); // Default unit: Fahrenheit
double outdoorTemp = TempSensor.getTemperature(outdoorAddr, DallasTemp::Unit::Celcius); // In Celsius
```

For prototyping purposes where you only have one sensor on the bus, you do not have to work with addresses at all.  You can instead use this method:
``` {.cpp}
double temperature = TempSensor.getTemperature();
```
Which internally makes an address request each time the method is called - hence it should only be used for prototyping as it is inefficient and thus slow (~1 sec).