#ifndef DALLASTEMP_H
#define DALLASTEMP_H
#include <arduino.h>
#include <limits.h>
const uint8_t bytesInTemperature = 9;
const uint8_t bytesInAddress = 8;

enum TemperatureUnit
{
   Celcius,
   Fahrenheit
};

enum TemperatureResolution
{
   Low,
   Standard,
   High,
   Ultra,
};

union SensorDataStatus
{
    uint8_t ErrorStatus;
        /*
        0x01 - Sensor never initializes.  STG or Open Circuit
        0x21 - Open circuit after initialization
        0x25 - Short to ground after initialization
        0x48 - Incorrect sensor address.  Occurs before initialization
        0x68 - Incorrect sensor address. Occurs after initialization
        0x4A - Short to power (after initialization)
        0x6A - Short to power (before initialization)
        */
    struct FailureModes
    {
        uint8_t SensorFailsToInitialize:1;
        uint8_t sensorShortToPower:1;
        uint8_t sensorNotResponding:1;
        uint8_t wrongAddress:1;
        uint8_t interruptedReading:1;
        uint8_t addressCRCfault:1;
        uint8_t temperatureCRCFault:1;
    } FailureModes;
};

struct TemperatureSensor
{
   float value;
   int16_t rawValue;
   TemperatureUnit unit;
   SensorDataStatus SensorDataStatus;
   uint8_t address[bytesInAddress];
   TemperatureSensor()
   {
      value = 0;
      rawValue = 0;
      unit = Fahrenheit;
      SensorDataStatus.ErrorStatus = false;
   }
};

class DallasTemp
{
public:
            DallasTemp(char pinNo = 8, TemperatureResolution resolution = Ultra);
   void     init(char pinNo);
   String   getAddress();
   void     getAddress(TemperatureSensor *Sensor);
   String   getAddresses(uint8_t addressCount);
   bool     getAddresses(TemperatureSensor *Sensor);
   void     getTemperature(TemperatureSensor *Sensor);
   void     setResolution(TemperatureResolution resolution);

private:
   enum PowerMode
   {
      PARASITIC,
      NON_PARASITIC
   };
   String      toHex(uint8_t inByte);
   void        reset(SensorDataStatus& sensorDataStatus);
   void        writeBit(bool outBit);
   void        writeByte(uint8_t outByte);
   bool        readBit();
   uint8_t     readByte();
   void        findPowerMode();
   bool        runCRC(uint8_t inputArr[], char arraySize);
   
   PowerMode powerMode;
   uint16_t  tConv;
   uint8_t   probePin;
   
   // ROM ADDRESSES
   const byte READ_ROM =   0x33;
   const byte MATCH_ROM =  0x55;
   const byte SKIP_ROM =   0xCC;
   const byte SEARCH_ROM = 0xF0;

   // SCRATCHPAD ADDRESSES
   const byte CONVERT_T =     0x44;
   const byte WRITE_SCRATCH = 0x4E;
   const byte READ_SCRATCH =  0xBE;
   const byte POWER_STATE =   0xB4;
};
#endif