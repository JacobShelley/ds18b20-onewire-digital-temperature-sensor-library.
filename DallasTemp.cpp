#include "DallasTemp.h"

// CONSTRUCTOR
// ***********
DallasTemp::DallasTemp(char pin, TemperatureResolution resolution)
{
   probePin = pin;
   findPowerMode();
   setResolution(resolution);
}


// ESTABLISH DEFAULT VALUES
// ************************
void DallasTemp::init(char pinNo)
{
   probePin = pinNo;
}


// START OF INITIALIZATION SEQUENCE
// ********************************
void DallasTemp::reset(SensorDataStatus& sensorDataStatus)
{
   digitalWrite(probePin, LOW);
   pinMode(probePin, OUTPUT);
   delayMicroseconds(500);
   
   pinMode(probePin, INPUT);
   delayMicroseconds(100);
   if (digitalRead(probePin))
   {
       sensorDataStatus.FailureModes.sensorShortToPower = true;
   }
   else
   {
       sensorDataStatus.FailureModes.sensorShortToPower = false;
   }
   delayMicroseconds(380);
}


// WRITE A SINGLE BIT
// ******************
void DallasTemp::writeBit(bool outBit)
{
   digitalWrite(probePin, LOW);
   pinMode(probePin, OUTPUT);
   switch (outBit)
   {
      case LOW:
      {
         delayMicroseconds(90); // 60us..120us
         digitalWrite(probePin, HIGH);
         delayMicroseconds(2); // 1 us min.
      }
      case HIGH:
      {
         delayMicroseconds(1); // 1us
         digitalWrite(probePin, HIGH);
         delayMicroseconds(60); // 55us..115us
      }
   }
}


// READ A SIGNLE BIT
// *****************
bool DallasTemp::readBit()
{
   digitalWrite(probePin, LOW);
    pinMode(probePin, OUTPUT);
   delayMicroseconds(1); // 1..15us

   pinMode(probePin, INPUT);
   delayMicroseconds(5); // so the pullup can do its job

   bool isBitHigh = digitalRead(probePin);
   delayMicroseconds(100); // 50us..110us
   return isBitHigh;
}


// WRITE A BYTE
// ************
void DallasTemp::writeByte(uint8_t outByte) // All commands are submitted LSB first (page 9)
{
   for (uint8_t i = 0; i < CHAR_BIT; i++)
   {
      writeBit(outByte >> i & 1);
   }
}


// READ A BYTE
// ***********
uint8_t DallasTemp::readByte()
{
   uint8_t inByte = 0x00;
   for (uint8_t i = 0; i < CHAR_BIT; i++)
   {
      inByte |= (readBit() << i);
   }
   return inByte;
}


// GET ADDRESSES AS A STRING
// *************************
String DallasTemp::getAddresses(uint8_t addressCount)
{
   TemperatureSensor Sensor;
   String stringOfAddresses;
   while (addressCount--)
   {
      stringOfAddresses += "{";
      getAddresses(&Sensor);
      for (int i = 0; i < bytesInAddress; i++)
      {
         if (i < 7) 
         {
            stringOfAddresses += "0x" + toHex(Sensor.address[i]) + ", ";
         }
         else 
         {
            stringOfAddresses += "0x" + toHex(Sensor.address[i]) + "} ";
            
            if (Sensor.SensorDataStatus.ErrorStatus == true)
            {
               stringOfAddresses += "Error: 0x" + toHex(Sensor.SensorDataStatus.ErrorStatus);
            }
            stringOfAddresses += "\n";
         }
      }
   };
   return stringOfAddresses;
 }

 
// GET ADDRESSES.  RETURNS FALSE IF THERE IS ANOTHER ADDRESS TO FIND.
// ******************************************************************
bool DallasTemp::getAddresses(TemperatureSensor *Sensor)
{
   bool bit;
   bool complement;
   int8_t bitLoc;
   static int8_t currentErrLocation = 64;
   static int8_t lastErrLocation = currentErrLocation;

   reset(Sensor->SensorDataStatus);
   writeByte(SEARCH_ROM);
   delay(5); // Probe needs processing time - this isn't shown in datasheet but won't work without it.

   // Walk through the address, bit by bit, LSB first.
   for (int8_t byteIndex = 0; byteIndex < bytesInAddress; byteIndex++)
   {
      Sensor->address[byteIndex] = 0;
      for (int8_t bitIndex = 0; bitIndex < CHAR_BIT; bitIndex++)
      {
         bitLoc = byteIndex * bitIndex;
         bit = readBit();        // Read bit
         complement = readBit(); // Read complement
         // If both bit and complement are low, then there's an address conflict with two probes.
         if (bitLoc < lastErrLocation)
         {
            if ((bit == LOW) && (complement == LOW))
            {
               currentErrLocation = bitLoc;
               bit = !bit;
            }
         }
         writeBit(bit);
         Sensor->address[byteIndex] |= bit << bitIndex;
      }
   }
   
   bool isCRCinvalid = runCRC(Sensor->address, bytesInAddress);
   if (isCRCinvalid) 
   {
      currentErrLocation = lastErrLocation; // currentErrLocation was changed above.  Revert if CRC is invalid.
      Sensor->SensorDataStatus.FailureModes.addressCRCfault = true;
   }
   else
   {
       Sensor->SensorDataStatus.FailureModes.addressCRCfault = false;
   }
   
   
   bool keepSearching = true;
   if (lastErrLocation == currentErrLocation)
   {
      currentErrLocation = 64;
      keepSearching = false;
   }
   lastErrLocation = currentErrLocation;
   
   return keepSearching;
}


// GET ADDRESS AS AN ARRAY
// ***********************
void DallasTemp::getAddress(TemperatureSensor *Sensor)
{
   reset(Sensor->SensorDataStatus);
   writeByte(READ_ROM);
   for (uint8_t i = 0; i < bytesInAddress; i++) 
   {
       Sensor->address[i] = readByte();
   }
   bool isCRCinvalid = runCRC(Sensor->address, bytesInAddress);
   if (isCRCinvalid)
   {
       Sensor->SensorDataStatus.FailureModes.addressCRCfault = true;
   }
   else
   {
       Sensor->SensorDataStatus.FailureModes.addressCRCfault = false;
   }
}


// GET ADDRESS AS A STRING
// ***********************
String DallasTemp::getAddress()
{
   TemperatureSensor *Sensor;
   getAddress(Sensor);
   String oneAddr = "{";
   for (uint8_t i = 0; i < bytesInAddress; i++)
   {
      oneAddr += "0x" + toHex(Sensor->address[i]);
      if (i != 7) oneAddr += ", ";
   }
   oneAddr += "} ";
   
   if (Sensor->SensorDataStatus.ErrorStatus == true)
   {
      oneAddr += "Error: 0x" + toHex(Sensor->SensorDataStatus.ErrorStatus);
   }
   return oneAddr + "\n";
}


// GET TEMPERATURE
// ***************
void DallasTemp::getTemperature(TemperatureSensor *Sensor)
{
   // If the user fails to set an address, and there is only one sensor on the bus,
   // then get that one address and proceed.
   if (Sensor->address[0] == 0) getAddress(Sensor);

   uint8_t tempArr[bytesInTemperature];

   // START TEMPERATURE CONVERSION PROCESS
   reset(Sensor->SensorDataStatus);
   writeByte(MATCH_ROM);
   for (int i = 0; i < bytesInAddress; i++) writeByte(Sensor->address[i]);
   writeByte(CONVERT_T);
   switch (powerMode)
   {
      case PARASITIC:
      {
         delay(tConv);
         break;
      }
      case NON_PARASITIC:
      {
         // Probe will pull pin high when ready to proceed
         unsigned long timeout = millis() + 2500UL; // 2.5sec
         while (!digitalRead(probePin) && (millis() < timeout)) {}
         if (millis() >= timeout) 
         {
             Sensor->SensorDataStatus.FailureModes.sensorNotResponding = true;
         }
         else
         {
             Sensor->SensorDataStatus.FailureModes.sensorNotResponding = false;
         }
         break;
      }
   }
   
   // READ TEMPERATURE FROM SCRATCHPAD
   reset(Sensor->SensorDataStatus);
   writeByte(MATCH_ROM);
   for (int i = 0; i < bytesInAddress; i++) 
   {
      writeByte(Sensor->address[i]);
   }
   writeByte(READ_SCRATCH);
   for (int i = 0; i < bytesInTemperature; i++) 
   {
      tempArr[i] = readByte();
   }
   
   // RUN CRC CHECK ON TEMPERATURE BITS
   bool isCRCinvalid = runCRC(tempArr, bytesInTemperature);
   
   Sensor->rawValue = tempArr[0] | ((uint16_t)tempArr[1] << CHAR_BIT);
      
   if (isCRCinvalid) 
   {
       Sensor->SensorDataStatus.FailureModes.temperatureCRCFault = true;
   }
   else
   {
       Sensor->SensorDataStatus.FailureModes.temperatureCRCFault = false;
   }
   
   switch (Sensor->rawValue)
   {
      case 0x0000:
      Sensor->SensorDataStatus.FailureModes.SensorFailsToInitialize = true;
      Sensor->SensorDataStatus.FailureModes.wrongAddress = false;
      Sensor->SensorDataStatus.FailureModes.interruptedReading = false;
      break;
       
      case 0xFFFF: // -0.0625°C
      Sensor->SensorDataStatus.FailureModes.wrongAddress = true;
      Sensor->SensorDataStatus.FailureModes.interruptedReading = false;
      Sensor->SensorDataStatus.FailureModes.SensorFailsToInitialize = false;
      break;
      
      case 0x0550: // 85°C
      Sensor->SensorDataStatus.FailureModes.interruptedReading = true;
      Sensor->SensorDataStatus.FailureModes.wrongAddress = false;
      Sensor->SensorDataStatus.FailureModes.SensorFailsToInitialize = false;
      break;
      
      default:
      Sensor->SensorDataStatus.FailureModes.wrongAddress = false;
      Sensor->SensorDataStatus.FailureModes.interruptedReading = false;
      Sensor->SensorDataStatus.FailureModes.SensorFailsToInitialize = false;
      break;
   }

   switch (Sensor->unit)
   {
      case Celcius:
      {
         Sensor->value = (float)Sensor->rawValue / 16.0; // 4-bit fixed-point mantissa.
         break;
      }
      case Fahrenheit:
      {
         Sensor->value = (float)Sensor->rawValue / 16.0 * 1.8 + 32; // Convert from C to F.
         break;
      }
   }  
}


// USER SELECTABLE RESOLUTION
// **************************
void DallasTemp::setResolution(TemperatureResolution resolution)
{
   SensorDataStatus sensorDataStatus;
   reset(sensorDataStatus);
   writeByte(SKIP_ROM);
   writeByte(WRITE_SCRATCH);
   writeByte(0); // High alarm (not used)
   writeByte(0); // Low alarm (not used)
   uint8_t resolutionConfigReg;
   switch (resolution)
   {
      case Low:
      {
         resolutionConfigReg = 0x1F; // Resolution register from datasheet: Fig 10.
         tConv = 94; // tConv values from datasheet: Table 2.
         break;
      }
      case Standard:
      {
         resolutionConfigReg = 0x3F;
         tConv = 188;
         break;
      }
      case High:
      {
         resolutionConfigReg = 0x5F;
         tConv = 375;
         break;
      }
      case Ultra:
      default:
      {
         resolutionConfigReg = 0x7F;
         tConv = 750;
         break;
      }
   }
   writeByte(resolutionConfigReg);
}


// TAKES A BYTE AND RETURNS ITS HEX VALUE
// **************************************
String DallasTemp::toHex(uint8_t inByte)
{
  char nybble[] = {inByte >> 4, inByte & 0x0F};
  for (uint8_t i = 0; i <= 1; i++)
  {
   if (nybble[i] < 10)
     nybble[i] += '0'; // hex: 0 - 9
   else
     nybble[i] += 'A' - 10; // hex: A - F
  }
  return String(nybble[0]) + String(nybble[1]);
}


// DISCOVERS IF THE SENSOR IS USING PARASITIC POWER
// ************************************************
void DallasTemp::findPowerMode()
{
   SensorDataStatus sensorDataStatus;
   reset(sensorDataStatus);
   writeByte(SKIP_ROM);
   writeByte(POWER_STATE);
   if (readBit())
      powerMode = NON_PARASITIC;
   else
      powerMode = PARASITIC;
}


/* CRC CHECK */
/*************/
bool DallasTemp::runCRC(uint8_t inputArr[], char arraySize) // FamilyCode..LSB..MSB..CRC
{
   static const uint8_t polynomial = 0x31; // 1 0011 0001 - 9 bits, ignore MSB
   uint8_t remainder = 0x00; // The dynamic, bitwise CRC computation

   for (uint8_t byteIndex = 0; byteIndex < arraySize; byteIndex++)
   {
      for (uint8_t bitIndex = 0; bitIndex < CHAR_BIT; bitIndex++)
      {
         if ((remainder >> 7) ^ (inputArr[byteIndex] >> bitIndex & 1))
         {
            remainder <<= 1;
            remainder ^= polynomial;
         }
         else
         {
            remainder <<= 1;
         }
      }
   }
   
   return (bool)remainder; // Returns 0 if inputArr has valid CRC.
}
